const express = require('express');
const app = express();
const port = 8000;
const nunjucks = require('nunjucks');
const path = require('path');
const session = require('express-session');
const { doubleCsrf } = require("csrf-csrf");
const cookieParser = require ("cookie-parser");

const bcrypt = require('bcrypt');
const saltRounds = 10;


const multer  = require('multer')
const upload = multer({ dest: "uploads/" });



app.use(express.urlencoded({ extended: true }));
app.use("/static", express.static( path.resolve(__dirname, 'static')));

// Serve uploads as static files
app.use("/uploads", express.static('uploads'));


app.use(session({
  secret: 'kerfuffle', 
  resave: false,    
  saveUninitialized: true,
  cookie: { secure: false } 
}));

// Middleware para asegurarse de que inicies sesión antes de tener la capacidad de escribir una queja
function protectedRoute(req, res, next){
  if(req.session.userId !== undefined){
    next();
  } else {
    res.send("Necesitas haber iniciado sesión");
  }
}

//configurar nunjucks
nunjucks.configure('templates', {
    autoescape: true,
    express: app
});

//CSRF
app.use(cookieParser("this is a secret"));

const csrf_config={
  getSecret: () => "Secret", // A function that optionally takes the request and returns a secret
  cookieName: "__Host-psifi.x-csrf-token", // The name of the cookie to be used, recommend using Host prefix.
  cookieOptions: {
    httpOnly: true,
    sameSite : "lax",  // Recommend you make this strict if posible
    path : "/",
  },
  size: 64, // The size of the generated tokens in bits
  ignoredMethods: ["GET", "HEAD", "OPTIONS"], // A list of request methods that will not be protected.
  getTokenFromRequest: (req) => req.body.csrf_token, // A function that returns the token from the request
};

const {
  invalidCsrfTokenError,
  generateToken,
  validateRequest,
  doubleCsrfProtection,
} = doubleCsrf(csrf_config);


/* mensaje flash no funciona
function flash(req, res, next){
  if(req.session && req.session.flash){
    req.locals = {};
    req.locals.flash = req.session.flash;
    delete req.session.flash;
  }
  next();
}
// Usar el middleware de flash
app.use(flash);*/



/*upload photos*/

app.post("/upload_files", doubleCsrfProtection, upload.array("files"), uploadFiles);

function uploadFiles(req, res) {
    console.log(req.body);
    console.log(req.files);
    res.json({ message: "Successfully uploaded files" });
}


/* conectar con mysql*/
const mysql = require('mysql');
const { resolve } = require('path');
const connection = mysql.createConnection ({
    host : 'localhost',
    database : 'quejas',
    user : 'quejas',
    password : 'passpass' // fallo de seguridad
});

connection.connect(function(err){
    if (err) {
        console.error('Connection error:' + err.stack); 
        return;
    }
    console.log ('Connected succesfully!'); 
});

/* usando async y await*/
app.get('/', async (_, res) => {
  try {
    const result = await new Promise((resolve, reject) => {
      connection.query('SELECT body, date FROM quejas', function (err, result, _) {
        if (err) {
          reject(err);
        }
        resolve(result);
      });
    });

    res.render('index.html', { quejas: result, dateFormat: (d) => d.toDateString()});
  } catch (err) {
    console.error(err);
    res.status(500).send('Internal Server Error');
  }
});

app.get('/login', (req, res)=>{
  const token = generateToken(res, req); // Ojo al orden de los argumentos
  res.render("login.html", {csrf_token: token});
});

app.get ('/nueva', protectedRoute, (_, res)=> {
  res.render ("nuevaqueja.html");
});

app.get ('/success', (_, res)=>{
  res.render ('success.html');
});

app.post ('/queja', protectedRoute, doubleCsrfProtection, (req, res)=> { /* va metiendo el cuerpo del mensaje y la fecha en la base de datos*/
    let data = {
        body: req.body.queja,
        date: new Date () 
    };
    connection.query ('INSERT INTO quejas  SET ?', data, function (err, _, _) { 
        if (err) {
            throw err;
        }
        console.log ('Añadido exitosamente!');
    });
    res.redirect('/');
}); 

app.post('/login', doubleCsrfProtection, (req, res) => {
  let usuario = req.body.usuario;
  let password = req.body.password;


  if (!usuario || !password) {
    return res.send('Por favor, introduce tus credenciales');
  }

  connection.query('SELECT * FROM usuarios WHERE ?', { usuario: usuario }, (err, result) => {
    if (err) throw err;
    if (result.length !== 1) {
        res.send('Usuario no encontrado');
        return;
    }
    bcrypt.compare(password, result[0].password, (err, correct) => { 
        if (err) throw err;
        if (correct) {
            req.session.regenerate((err) => { if (err) throw err; });
            req.session.userId = result[0].id;
            req.session.save(function (err) {
                if (err) throw err;
                console.log('Credenciales correctas');
                res.redirect('/');
            });
        } else {
            res.send('Credenciales incorrectas');
        }
    });
});
});

app.post('/newuser', upload.single ('imagen'), doubleCsrfProtection,  (req, res) => {
  bcrypt.hash(req.body.password, saltRounds, (err, hash) => {
      if (err) throw err;
      let data = {
          usuario: req.body.usuario,
          password: hash // guarda la contraseña hasheada
      }
      connection.query('INSERT INTO usuarios SET ?',
          data,
          (err, _) => {
              if (err) throw err;
              console.log('Usuario registrado');
              res.send('Usuario registrado');
          });
      console.log (req.file);
      connection.query('INSERT INTO avatar SET ?', { avatar: req.file.path }, (err, _) => { // path es el campo de 'file' que tiene la URL de la imagen
          if (err) throw err; 
      });
  });
});

app.get ('/logout', (_, res) => {
  res.render ('##');
});

app.get('/newuser', (req, res) => {
  const token = generateToken(res, req);
  res.render('newuser.html', {csrf_token: token});
});

app.listen(port, () => {
console.log(`Example app listening on port ${port}`)
});


/* what are migrations?

In programming, migrations refer to the process of managing changes to the structure of a database schema. 
These changes can include adding or removing tables, columns, or indexes, as well as modifying 
the data type of a column or changing a relationship between tables.

Migrations are often used in web development when creating and updating web applications 
that require a database to store and manage data. By using migrations, 
developers can ensure that their changes to the database schema are tracked, versioned, 
and applied in a consistent and automated way across multiple environments (such as development, staging, and production).

Migrations typically involve creating a script or set of scripts that define the changes to the database schema. 
These scripts can then be executed using a database migration tool, 
which will apply the changes to the database and update the schema accordingly.*/
